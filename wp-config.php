<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'testdb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9)-Ev~>mTr0HL$[U[-5gn :nlut2=60v?PV,d3R. 32mv3M+O7?oSmee+egS3US&' );
define( 'SECURE_AUTH_KEY',  '.$,Y%~jO!+A}BHQz(nTvdu+^04P43o-YoQX`L{&2:}QWvB4/}OB_@Izh}}eDA;DC' );
define( 'LOGGED_IN_KEY',    '{kE<S0pRp$Et3[RGpj^ZO;3T:Npg6cTwWM|@3$aQi)S#/wKG6Z9qUyj6RN]$s>L|' );
define( 'NONCE_KEY',        'EliUYE9f+Ag+;$8`_kbLr;:=u.lmJc+4+q!)dHw/,!XU,tB{^N%K5$7-*5 B,4vf' );
define( 'AUTH_SALT',        'bA$pmZp%Xu*4oOPD83#UK~2tew.)z=,2=Vi0X`Q?8S05JXxN-h/ | j,E%L#|[s9' );
define( 'SECURE_AUTH_SALT', '/~3SyVY/j@?hZAJzF[+x@%z+]f[2!gwKB7T&&|:$_1$7na|5w`Mrl5t5 80eD~fE' );
define( 'LOGGED_IN_SALT',   '5>&/l(j-oJ6?=t=6}moOmxVS`2;CpFE^rw?,*l98&rcD)Zo@.M;`ZzfSGnd(GCdK' );
define( 'NONCE_SALT',       'S?y:_2Y;}b}xGs*j%Vy_RI#FwywQ0[ fSO.[<~t9YXNzH7=bKlZ@>G$|9G`gqI[q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define('VP_ENVIRONMENT', 'default');
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
